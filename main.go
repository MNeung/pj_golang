package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"golang.org/x/crypto/bcrypt"
)

type User struct {
	Username  string `json:"username"`
	Password  string `json:"password"`
	FirstName string `json:"firstname"`
	LastName  string `json:"lastname"`
	Token     string `json:"token"`
}

type CPassword struct {
	Username        string `json:"username"`
	CurrentPassword string `json:"currentPassword"`
	NewPassword     string `json:"newPassword"`
}

// type User struct {
// 	Username  string `json:"username,omitempty" bson:"username,omitempty"`
// 	Password  string `json:"password,omitempty" bson:"password,omitempty"`
// 	FirstName string `json:"firstname,omitempty" bson:"firstname,omitempty"`
// 	LastName  string `json:"lastname,omitempty" bson:"lastname,omitempty"`
// 	Token     string `json:"token,omitempty" bson:"token,omitempty"`
// }

type ResponseResult struct {
	Error  string `json:"error"`
	Result string `json:"result"`
}

type TodosData struct {
	Username string `json:"username"`
	Todos    string `json:"todos`
}

type ClientTodo struct {
	IDTodo    string `json:"id"`
	ValueTodo string `json:"valueTodo"`
}

type DBTodoList struct {
	ID       string `json:"_id"`
	Username string `json:"username"`
	Todo     string `json:"todos"`
}

type TodosList struct {
	ID    string `json:"id"`
	Value string `json:"value"`
}

var client *mongo.Client

// var mySigningKey = []byte("mysupersecretphrase")

// func GenerateJWT() (string, error) {
// 	token := jwt.New(jwt.SigningMethodHS256)

// 	claims := token.Claims.(jwt.MapClaims)

// 	claims["authorized"] = true
// 	claims["user"] = "Elliot Forbes"
// 	claims["exp"] = time.Now().Add(time.Minute * 30).Unix()

// 	tokenString, err := token.SignedString(mySigningKey)

// 	if err != nil {
// 		fmt.Errorf("Something went wrong: %s", err.Error)
// 		return "", err
// 	}

// 	return tokenString, nil
// }

func Register(response http.ResponseWriter, request *http.Request) {
	response.Header().Add("content-type", "application/json")
	var user User
	body, _ := ioutil.ReadAll(request.Body)
	err := json.Unmarshal(body, &user)
	var res ResponseResult
	if err != nil {
		res.Error = err.Error()
		json.NewEncoder(response).Encode(res)
		return
	}
	collection := client.Database("test_golang").Collection("users")
	if err != nil {
		res.Error = err.Error()
		json.NewEncoder(response).Encode(res)
		return
	}
	// ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	// result, _ := collection.InsertOne(ctx, user)
	// json.NewEncoder(response).Encode(result)
	// fmt.Println("req", request.Body)

	var result User
	err = collection.FindOne(context.TODO(), bson.D{{"username", user.Username}}).Decode(&result)
	fmt.Println("result register", result)
	if err != nil {
		if err.Error() == "mongo: no documents in result" {
			hash, err := bcrypt.GenerateFromPassword([]byte(user.Password), 5)

			if err != nil {
				res.Error = "Error While Hashing Password, Try Again"
				json.NewEncoder(response).Encode(res)
				return
			}
			user.Password = string(hash)

			_, err = collection.InsertOne(context.TODO(), user)
			if err != nil {
				res.Error = "Error While Creating User, Try Again"
				json.NewEncoder(response).Encode(res)
				return
			}
			res.Result = "Registration Successful"
			json.NewEncoder(response).Encode(res)
			return
		}

		res.Error = err.Error()
		json.NewEncoder(response).Encode(res)
		return
	}

	res.Result = "Username already Exists!!"
	json.NewEncoder(response).Encode(res)
	return
}

// func Register(response http.ResponseWriter, request *http.Request) {
// 	response.Header().Set("content-type", "application/json")
// 	var user User
// 	_ = json.NewDecoder(request.Body).Decode(&user)
// 	collection := client.Database("test_golang").Collection("users")
// 	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
// 	result, _ := collection.InsertOne(ctx, user)
// 	json.NewEncoder(response).Encode(result)
// }

func Login(response http.ResponseWriter, request *http.Request) {
	response.Header().Add("content-type", "application/json")
	var user User
	body, _ := ioutil.ReadAll(request.Body)
	err := json.Unmarshal(body, &user)
	if err != nil {
		log.Fatal(err)
	}

	collection := client.Database("test_golang").Collection("users")
	if err != nil {
		log.Fatal(err)
	}

	var result User
	var res ResponseResult

	err = collection.FindOne(context.TODO(), bson.D{{"username", user.Username}}).Decode(&result)

	if err != nil {
		res.Error = "Invalid username"
		json.NewEncoder(response).Encode(res)
		return
	}

	err = bcrypt.CompareHashAndPassword([]byte(result.Password), []byte(user.Password))

	if err != nil {
		res.Error = "Invalid password"
		json.NewEncoder(response).Encode(res)
		return
	}

	fmt.Println("err login", err)

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"username":  result.Username,
		"firstname": result.FirstName,
		"lastname":  result.LastName,
	})

	tokenString, err := token.SignedString([]byte("secret"))

	// fmt.Println("tokenString", tokenString)

	if err != nil {
		res.Error = "Error while generating token,Try again"
		json.NewEncoder(response).Encode(res)
		return
	}

	result.Token = tokenString
	result.Password = ""

	// fmt.Println("result login =>", result)

	json.NewEncoder(response).Encode(result)
}

func ChangePassword(response http.ResponseWriter, request *http.Request) {
	fmt.Println("change password")
	response.Header().Add("content-type", "application/json")
	var cpassword CPassword
	body, _ := ioutil.ReadAll(request.Body)
	err := json.Unmarshal(body, &cpassword)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("cpassword", cpassword)

	collection := client.Database("test_golang").Collection("users")
	if err != nil {
		log.Fatal(err)
	}

	var result User
	var res ResponseResult

	err = collection.FindOne(context.TODO(), bson.D{{"username", cpassword.Username}}).Decode(&result)

	if err != nil {
		res.Error = "Invalid username"
		json.NewEncoder(response).Encode(res)
		return
	}

	fmt.Println("result", result)

	err = bcrypt.CompareHashAndPassword([]byte(result.Password), []byte(cpassword.CurrentPassword))

	if err != nil {
		res.Error = "Invalid password"
		json.NewEncoder(response).Encode(res)
		return
	}

	hash, err := bcrypt.GenerateFromPassword([]byte(cpassword.NewPassword), 5)

	if err != nil {
		res.Error = "Error While Hashing Password, Try Again"
		json.NewEncoder(response).Encode(res)
		return
	}
	cpassword.NewPassword = string(hash)

	fmt.Println("errj", err)
	fmt.Println("===============")

	filter := bson.D{{"username", cpassword.Username}}
	update := bson.D{
		{"$set", bson.D{
			{"password", cpassword.NewPassword},
		}},
	}
	_, err = collection.UpdateMany(
		context.TODO(),
		filter,
		update,
	)
	if err != nil {
		res.Error = err.Error()
		json.NewEncoder(response).Encode(res)
		return
	}

}

func Logout(response http.ResponseWriter, request *http.Request) {
	response.Header().Add("content-type", "application/json")
	// clearSession(response)
	// http.Redirect(response, request, "/", 302)
	fmt.Println("response", response)
	fmt.Println("request", request)
}

// func clearSession(response http.ResponseWriter) {
// 	cookie := &http.Cookie{
// 		Name:   "session",
// 		Value:  "",
// 		Path:   "/",
// 		MaxAge: -1,
// 	}
// 	http.SetCookie(response, cookie)
// }

func Profile(response http.ResponseWriter, request *http.Request) {
	response.Header().Add("content-type", "application/json")
	// tokenString := request.Header.Get("Authorization")
	// fmt.Println("tokenStrin", tokenString)
	// token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
	// 	// Don't forget to validate the alg is what you expect:
	// 	if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
	// 		return nil, fmt.Errorf("Unexpected signing method")
	// 	}
	// 	return []byte("secret"), nil
	// })
	// var result User
	// var res ResponseResult
	// if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
	// 	result.Username = claims["username"].(string)
	// 	result.FirstName = claims["firstname"].(string)
	// 	result.LastName = claims["lastname"].(string)

	// 	json.NewEncoder(response).Encode(result)
	// 	return
	// } else {
	// 	res.Error = err.Error()
	// 	json.NewEncoder(response).Encode(res)
	// 	return
	// }
}

func TodosListService(response http.ResponseWriter, request *http.Request) {
	response.Header().Add("content-type", "application/json")
	vars := mux.Vars(request)
	id := vars["id"]
	response.WriteHeader(http.StatusOK)

	collection := client.Database("test_golang").Collection("todos")

	opts := options.Find()
	opts.SetSort(bson.D{{"username", -1}})
	sortCursor, err := collection.Find(context.TODO(), bson.D{{"username", id}}, opts)
	if err != nil {
		log.Fatal(err)
	}

	var episodesSorted []bson.M
	if err = sortCursor.All(context.TODO(), &episodesSorted); err != nil {
		log.Fatal(err)
	}
	json.NewEncoder(response).Encode(episodesSorted)
}

func AddTodoService(response http.ResponseWriter, request *http.Request) {
	response.Header().Add("content-type", "application/json")
	var todosdata TodosData
	body, _ := ioutil.ReadAll(request.Body)
	err := json.Unmarshal(body, &todosdata)
	var res ResponseResult
	if err != nil {
		res.Error = err.Error()
		json.NewEncoder(response).Encode(res)
		return
	}
	collection := client.Database("test_golang").Collection("todos")
	if err != nil {
		res.Error = err.Error()
		json.NewEncoder(response).Encode(res)
		return
	}

	var resultTodo TodosData
	err = collection.FindOne(context.TODO(), bson.D{{"username", todosdata.Username}}).Decode(&resultTodo)
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	result, err := collection.InsertOne(ctx, todosdata)
	if err != nil {
		res.Error = err.Error()
		json.NewEncoder(response).Encode(res)
		return
	}
	// fmt.Print("result add =>", result)
	json.NewEncoder(response).Encode(result)
}

func UpdateTodoService(response http.ResponseWriter, request *http.Request) {
	response.Header().Add("content-type", "application/json")
	var clientTodo ClientTodo
	body, _ := ioutil.ReadAll(request.Body)
	err := json.Unmarshal(body, &clientTodo)
	fmt.Println("clientTodo", clientTodo)
	fmt.Println("err", err)
	var res ResponseResult
	if err != nil {
		res.Error = err.Error()
		json.NewEncoder(response).Encode(res)
		return
	}
	collection := client.Database("test_golang").Collection("todos")
	if err != nil {
		res.Error = err.Error()
		json.NewEncoder(response).Encode(res)
		return
	}

	// objID, err := primitive.ObjectIDFromHex(clientTodo.IDUser)
	// if err != nil {
	// 	fmt.Println(err)
	// 	return
	// }

	// fmt.Println("objID", objID)

	id, _ := primitive.ObjectIDFromHex(clientTodo.IDTodo)
	var resultTodo TodosData
	// err = collection.FindOne(context.TODO(), bson.D{{"_id", id}}).Decode(&resultTodo)
	// if err != nil {
	// 	res.Error = err.Error()
	// 	json.NewEncoder(response).Encode(res)
	// 	return
	// }
	// fmt.Println("err", err)

	// fmt.Println("errUser", value)
	// fmt.Println("resultTodo", resultTodo)
	filter := bson.D{{"_id", id}}
	update := bson.D{
		{"$set", bson.D{
			{"todos", clientTodo.ValueTodo},
		}},
	}
	_, err = collection.UpdateMany(
		context.TODO(),
		filter,
		update,
	)
	// // .Decode(&resultTodo)
	if err != nil {
		res.Error = err.Error()
		json.NewEncoder(response).Encode(res)
		return
	}

	fmt.Println("resultTodo", err)

	fmt.Println("len:", len(resultTodo.Todos))
	fmt.Println("===========")

	// ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	// result, _ := collection.InsertOne(ctx, todosdata)
	json.NewEncoder(response).Encode(resultTodo)
}

func DeleteTodoService(response http.ResponseWriter, request *http.Request) {
	response.Header().Add("content-type", "application/json")
	vars := mux.Vars(request)
	id := vars["id"]
	valueId, _ := primitive.ObjectIDFromHex(id)
	response.WriteHeader(http.StatusOK)
	database := client.Database("test_golang")
	podcastsCollection := database.Collection("todos")
	result, err := podcastsCollection.DeleteOne(context.TODO(), bson.M{"_id": valueId})
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("DeleteOne removed %v document(s)\n", result.DeletedCount)
	json.NewEncoder(response).Encode(result)
}

func main() {
	fmt.Println("Starting the application...")

	// tokenString, err := GenerateJWT()
	// if err != nil {
	// 	fmt.Println("Error generating token string")
	// }
	// fmt.Println(tokenString)

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	clientOptions := options.Client().ApplyURI("mongodb://localhost:27017")
	client, _ = mongo.Connect(ctx, clientOptions)
	// /// Set client options
	// client, err := mongo.NewClient(options.Client().ApplyURI("mongodb://localhost:27017"))
	// if err != nil {
	// 	log.Fatal(err)
	// }

	// // Connect to MongoDB
	// ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	// err = client.Connect(ctx)
	// if err != nil {
	// 	log.Fatal(err)
	// }
	router := mux.NewRouter()
	headers := handlers.AllowedHeaders([]string{"X-Requested-With", "Content-Type", "Authorization"})
	methods := handlers.AllowedMethods([]string{"GET", "POST", "PUT", "DELETE"})
	origins := handlers.AllowedOrigins([]string{"*"})
	router.HandleFunc("/api/register", Register).Methods("POST")
	router.HandleFunc("/api/login", Login).Methods("POST")
	router.HandleFunc("/api/changepass", ChangePassword).Methods("PUT")
	router.HandleFunc("/api/logout", Logout).Methods("POST")
	router.HandleFunc("/api/profile", Profile).Methods("GET")
	router.HandleFunc("/api/todos/{id}", TodosListService).Methods("GET")
	router.HandleFunc("/api/todo", AddTodoService).Methods("POST")
	router.HandleFunc("/api/updatetodo", UpdateTodoService).Methods("PUT")
	router.HandleFunc("/api/deletetodo/{id}", DeleteTodoService).Methods("DELETE")
	http.ListenAndServe(":8000", handlers.CORS(headers, methods, origins)(router))
	// defer client.Disconnect(ctx)

	// Check the connection
	// err = client.Ping(ctx, readpref.Primary())
	// databases, err := client.ListDatabases(ctx, bson.M{})
	// if err != nil {
	// 	log.Fatal(err)
	// }

	// fmt.Println("Connected to MongoDB!")

	//look database
	// quickstartDatabase := client.Database("test_golang")
	//look collection
	// usersCollection := quickstartDatabase.Collection("users")

	// userdata := User{"Test6", ""}
	//add data database
	// userResult, err := usersCollection.InsertOne(ctx, userdata)
	// fmt.Println("userResult", userResult)

	//delete data database
	// deleteResult, err := usersCollection.DeleteMany(context.TODO(), bson.D{{}})
	// if err != nil {
	// 	log.Fatal(err)
	// }
	// fmt.Printf("Deleted %v documents in the trainers collection\n", deleteResult.DeletedCount)

	// err = client.Disconnect(context.TODO())

	// if err != nil {
	// 	log.Fatal(err)
	// }
	// fmt.Println("Connection to MongoDB closed.")
}
